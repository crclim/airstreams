import setuptools

try:
    from numpy.distutils.core import setup, Extension
except:
    raise ImportError('AirStreams requires Numpy')

# setup fortran 90 extension
# ---------------------------------------------------------------------------
f90_fnames = ['hierarchical_clustering.f90']

f90_paths = []
for fname in f90_fnames:
    f90_paths.append('airstreams/libs/' + fname)

ext = Extension(
    name='airstreams.libs.hierarchical_clustering',
    sources=f90_paths,
    extra_f90_compile_args=["-O3", "-fbounds-check"] #, "-fopenmp"],
    # extra_link_args=["-lgomp"],
)

# write short description
# --------------------------------------------------------------------------
description = 'AirStreams'

# puts the contents of the README file in the variable long_description
# --------------------------------------------------------------------------
# with open('README.txt') as file:
#     long_description = '\n\n ' + file.read()
long_description = description

setup(
    setup_requires=['numpy'],
    install_requires=['numpy', 'netCDF4'],

    name='AirStreams',
    version='0.1.0',
    description=description,
    long_description=long_description,
    license='GPL v3',
    platforms='linux',
    author='Nicolas Piaget',
    author_email='nicolas.piaget@env.ethz.ch',
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Fortran",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
        "Topic :: Scientific/Engineering :: Meteorology",
        "Intended Audience :: Science/Research",
        "Development Status :: 4 - Beta",
        "Topic :: Education",
        "Natural Language :: English",
    ],

    packages=setuptools.find_packages(),
    package_data={'': ['*.f90'],
                  'airstreams.data': ['*.cdf']},
    include_package_data=True,
    ext_modules=[ext],
)

# coding: utf-8
from airstreams.airstreams import return_flows
from dypy.lagranto import Tra
from path import Path
import numpy as np

dirname = Path(__file__).parent.realpath()
trajsfile = Path.joinpath(dirname, 'test_data', 'traj_cosmo7.nc')


def test_number():
    filename = Path.joinpath(dirname, '..', 'airstreams', 'data',
                             'startf.txt')
    lonlat = np.genfromtxt(filename, names=['lon', 'lat'],
                           dtype=['f8', 'f8'])
    trajs = Tra(trajsfile, unit='seconds')
    flows = return_flows(trajs)
    for flow in flows:
        np.testing.assert_allclose(flow['lon'][:, 0], lonlat['lon'][
            flow['number'][:, 0].astype(int)], atol=1e-4)

"""tests
    Test the characteristics of Flows
"""
import numpy as np
import pytest
from airstreams import Flow
from dypy.lagranto import Tra
from path import Path
from airstreams.libs.tools import calculate_normal_vectors, composante_along_normal


@pytest.fixture()
def flow():
    flow = Flow()
    flow.set_array(
        np.array([[(0, 0, 0, 1, 1, 0),
                   (1, 0, 1, 1, 1, 0),
                   (2, 0, 2, 1, 1, 0),
                   (3, 0, 3, 1, 1, 0),
                   (4, 0, 4, 1, 1, 0)],
                  [(0, 1, 0, 1, 2, 1),
                   (1, 1, 1, 1, 1, 1),
                   (2, 1, 2, 1, 1, 1),
                   (3, 1, 3, 1, 1, 1),
                   (4, 1, 4, 1, 1, 1)
                   ],
                  [(0, 2, 0, 2, 1, 2),
                   (1, 2, 1, 1, 1, 2),
                   (2, 2, 2, 1, 1, 2),
                   (3, 2, 3, 1, 1, 2),
                   (4, 2, 4, 1, 1, 2)
                   ],
                  [(0, 3, 0, -1, 1, 3),
                   (1, 3, 1, -1, 1, 3),
                   (2, 3, 2, -1, 1, 3),
                   (3, 3, 3, -1, 1, 3),
                   (4, 3, 4, -1, 1, 3)
                   ]],
              dtype=[('time', '<f4'), ('lon', '<f4'), ('lat', '<f4'),
                     ('U', '<f4'), ('V', '<f4'), ('number', '<f4')])
    )
    return flow


@pytest.fixture
def normals():
    points = np.array([[0, 0], [1, 0], [2, 0], [3, 0], [4, 0]])
    normal_vectors = calculate_normal_vectors(points=points,
                                              filename='test.npy',
                                              redo=True)
    Path('test.npy').remove()
    return normal_vectors


def test_wind_normal(flow, normals):
    wind = np.stack((flow['U'][:, 0], flow['V'][:, 0]), axis=1)
    normal_winds = [composante_along_normal(n, w)
                    for n, w in zip(normals, wind)]
    assert normal_winds == [1., 2., 1., 1.]
    return normal_winds


def test_angle_normal(flow, normals):
    wind = np.stack((flow['U'][:, 0], flow['V'][:, 0]), axis=1)
    norm = np.linalg.norm(wind, axis=1)
    adjac = test_wind_normal(flow, normals)
    opp = norm * np.sin(np.arccos(adjac / norm))
    sign = np.sign(normals[:-1, 0]*wind[:, 1] - normals[:-1, 1]*wind[:, 0])
    angles = sign * 180 * np.arctan2(adjac, opp) / np.pi
    good_angles = np.array([-45.000, -63.434, -26.565, 45.000])
    np.testing.assert_almost_equal(angles, good_angles, decimal=3)


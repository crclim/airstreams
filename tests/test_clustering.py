# coding: utf-8
"""tests
    Test the differents part of the clustering algorithm
"""
import numpy as np
import pytest
from scipy.cluster._hierarchy import linkage
from scipy.spatial.distance import pdist

from airstreams.libs import hc, dissmatrix, great_vect


@pytest.fixture()
def lonlat():
    return np.array([[[10, 45], [11, 45], [12, 45], [13, 45]],
                     [[10, 46], [11, 46], [12, 46], [13, 46]],
                     [[10, 47], [11, 47], [12, 47], [13, 47]],
                     [[10, 48], [11, 48], [12, 48], [13, 48]],
                     [[10, 49], [11, 49], [12, 49], [13, 49]]])


def great_dist(traj1, traj2):
    return great_vect(traj1[::2], traj1[1::2], traj2[::2], traj2[1::2]).sum()


def test_dissmatrix(lonlat):
    distances_f = dissmatrix(lonlat)
    distances = pdist(lonlat.reshape(lonlat.shape[0], -1), great_dist)
    assert abs(distances_f - distances).max() < 10


def ia_ib_crit_to_z(ia, ib, crit):
    lia = len(ia)
    z = np.ones((lia - 1, 4))
    iab = {}
    for i, (a, b, c) in enumerate(zip(ia, ib, crit)):
        item = [0, 0, 0, 0]
        if int(i) == lia - 1:
            continue
        item[0], counta = iab.get(a, (a - 1, 1))
        item[1], countb = iab.get(b, (b - 1, 1))
        item[2] = c
        item[3] = counta + countb
        if item[1] < item[0]:
            item[:2] = item[1], item[0]
        iab[a] = (i + lia, counta + countb)
        iab[b] = (i + lia, counta + countb)
        # print('\n')
        z[i, :] = item
    return z


def test_z(lonlat):
    ntra = lonlat.shape[0]
    distances = pdist(lonlat.reshape(ntra, -1), great_dist)
    ia, ib, crit = hc(ntra, 1, distances)
    z_f = ia_ib_crit_to_z(ia, ib, crit)
    z = np.zeros((ntra - 1, 4))
    linkage(distances, z, ntra, 5)
    assert np.testing.assert_almost_equal(z, z_f, decimal=0)

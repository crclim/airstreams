from airstreams.libs.tools import normal_point, calculate_normal_vectors
import numpy as np
from path import Path


def test_normal_point():
    pt1 = [0, 2]
    pt2 = [0, 0]
    np.testing.assert_equal(normal_point(pt1, pt2), np.array([2, 0]))


def test_calculate_normal_vectors():
    points = np.array([[0, 4], [0, 3], [0, 2], [0, 1], [0, 0]])
    normal_vectores = calculate_normal_vectors(points=points,
                                               filename='test.npy',
                                               redo=True)
    Path('test.npy').remove()
    control_vectores = np.array([[1, 0], [1, 0], [1, 0], [1, 0], [-4, 0]])
    np.testing.assert_equal(normal_vectores, control_vectores)

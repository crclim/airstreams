import matplotlib.pyplot as plt
import numpy as np
import pytest
from PIL import Image
from airstreams import Flow
from airstreams.gridding import gridding
from dypy.netcdf import read_var
from dypy.plotting import Mapfigure
from dypy.lagranto import Tra

from path import path

testdatadir = path.dirname(path(__file__)).joinpath('test_data')


@pytest.fixture()
def trajs():
    trajs = Tra()
    lon = np.array([[0, 5]])
    lon = np.core.records.fromarrays(lon, names='lon', formats='f8')
    lon = lon.reshape((1, -1))
    lat = np.array([[45, 45]])
    time = np.array([[1, 2]])
    trajs.set_array(lon)
    trajs['lat'] = lat
    trajs['time'] = time
    return trajs


@pytest.fixture()
def flow(trajs):
    flow = Flow()
    flow.set_array(trajs)
    return flow


@pytest.fixture()
def m():
    return Mapfigure(domain=[-10, 15, 35, 55])


def results(n, filename, m):
    picfile = testdatadir.joinpath(filename)
    fig = plt.figure()
    m.drawcoastlines()
    lon = np.linspace(-180, 180, n.shape[-1])
    lat = np.linspace(-90, 90, n.shape[0])
    lons, lats = np.meshgrid(lon, lat)
    m.pcolormesh(lons, lats, n, latlon=True)

    if picfile.isfile():
        ctrl_array = np.array(Image.open(picfile))
        fig.savefig(filename, bbox_inches='tight')
        test_array = np.array(Image.open(filename))
        np.testing.assert_almost_equal(ctrl_array, test_array)
        path(filename).remove()
    else:
        fig.savefig(picfile, bbox_inches='tight')


def test_default(trajs, m):
    filename = 'test_default.png'

    outfile = path('test_default')
    gridding(trajs, outfile, var='time')

    outfile += '.cdf'
    cstfile = path(outfile + '_cst')
    n, = read_var(outfile, 'N')
    results(n, filename, m)
    outfile.remove()
    cstfile.remove()


def test_highres(trajs, m):
    outfile = path('test_highres')
    filename = outfile + '.png'
    gridding(trajs, outfile, var='time', dx=0.25, dy=0.25)
    outfile += '.cdf'
    cstfile = path(outfile + '_cst')
    n, = read_var(outfile, 'N')
    results(n, filename, m)
    outfile.remove()
    cstfile.remove()


def test_average(trajs, m):
    outfile = path('test_average')
    filename = outfile + '.png'
    gridding(trajs, outfile, var='time', options='-a')
    outfile += '.cdf'
    cstfile = path(outfile + '_cst')
    n, = read_var(outfile, 'N')
    results(n, filename, m)
    outfile.remove()
    cstfile.remove()


def test_filtering(trajs, m):
    outfile = path('test_filtering')
    filename = outfile + '.png'
    gridding(trajs, outfile, var='time', options='-f')
    outfile += '.cdf'
    cstfile = path(outfile + '_cst')
    n, = read_var(outfile, 'N')
    results(n, filename, m)
    outfile.remove()
    cstfile.remove()


def test_flow_gridding(trajs, m):
    outfile = path('test_flow_gridding')
    filename = outfile + '.png'
    gridding(trajs, outfile, var='time')
    outfile += '.cdf'
    cstfile = path(outfile + '_cst')
    n, = read_var(outfile, 'N')
    results(n, filename, m)
    outfile.remove()
    cstfile.remove()

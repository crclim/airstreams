import numpy as np

from .libs.hierarchical_clustering import mhc


class HCError(Exception):
    pass


class HierarchicalClustering:
    """
    Perform Hierarchical clustering on the <distance> matrix.

    The linkage can be one of the following:
        ward
        single
        complete
        average
        mcquitty
        median (Gower)
        centroid

    """
    _linkages = ['ward', 'single', 'complete', 'average', 'mcquitty',
                 'median', 'centroid']
    _methods = {'standard': [mhc.dissmatrix, ['lon', 'lat']],
                'height': [mhc.hdissmatrix, ['lon', 'lat', 'z']],
                'sheight': [mhc.hsdissmatrix, ['lon', 'lat', 'z']],
                }

    def __init__(self, ncluster=3, linkage='ward', distances=None,
                 method='standard'):

        try:
            self.linkage = linkage
            self._linkagen = self._linkages.index(linkage) + 1
        except ValueError:
            errormsg = '{} is not available, please choose from {}'
            errormsg = errormsg.format(linkage, ', '.join(self._linkages))
            raise HCError(errormsg)

        self.method = method
        self.distances = distances
        self.ncluster = ncluster
        self._ia = None
        self._ib = None
        self._crit = None
        self._iclass = None
        self.labels_ = None

    def _get_distances(self, trajs):
        method, variables = self._methods[self.method]
        ntrajs = trajs[variables].view(trajs.dtype[1])
        ntrajs = ntrajs.reshape(trajs.shape + (-1,))
        self.distances = method(ntrajs)

    def fit(self, trajs):
        """Fit the hierachical clustering on the trajectories (Tra object)

           Return self
        """
        if self.distances is None:
            self._get_distances(trajs)
        self._ia, self._ib, self._crit = mhc.hc(trajs.ntra, self._linkagen,
                                                self.distances)
        self._ia_ib_crit_to_z()
        self._iclass = mhc.hclass(self._ia, self._ib, self._crit, self.ncluster)
        self.labels_ = self._iclass[:, -2]
        # TODO limit trajectories to 1000km interpolate every 100km
        #       cluster < 1000km + clusters > 1000km
        #       (done using automatic clustering)
        #       http://stackoverflow.com/questions/21638130/
        #       tutorial-for-scipy-cluster-hierarchy
        #       knee = np.diff(z[::-1, 2], 2)
        #       num_clust1 = knee.argmax() + 2

    def fit_predict(self, trajs):
        """Fit the hierachical clustering on the data and return the labels"""
        self.fit(trajs)
        return self.labels_

    def _ia_ib_crit_to_z(self):
        lia = len(self._ia)
        z = np.ones((lia - 1, 4))
        iab = {}
        for i, (a, b, c) in enumerate(zip(self._ia, self._ib, self._crit)):
            item = [0, 0, 0, 0]
            if int(i) == lia - 1:
                continue
            item[0], counta = iab.get(a, (a - 1, 1))
            item[1], countb = iab.get(b, (b - 1, 1))
            item[2] = c
            item[3] = counta + countb
            if item[1] < item[0]:
                item[:2] = item[1], item[0]
            iab[a] = (i + lia, counta + countb)
            iab[b] = (i + lia, counta + countb)
            z[i, :] = item
        self.Z = z

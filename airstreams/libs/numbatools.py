import math

import numba as nb

__all__ = ['great_vect']


@nb.vectorize([nb.float64(nb.float64, nb.float64, nb.float64, nb.float64)])
def great_vect(lon1, lat1, lon2, lat2):
    """
    return the distance (km) between points following a great circle

    based on : https://gist.github.com/gabesmed/1826175

    >>> great_vect(0, 55, 8, 45.5)
    1199.3240879770135
    """

    earth_circumference = 6378137     # earth circumference in meters

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = earth_circumference * c

    return distance/1000

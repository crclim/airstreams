# coding: utf-8
import numpy as np
from shapely.geometry import LineString
from shapely.geometry import Polygon

from airstreams.plotting import return_startf
from dypy.intergrid import Intergrid
from dypy.netcdf import read_var
from path import Path
import cartopy.crs as ccrs

__all__ = ['great_vect', 'row_col_from_condensed_index']


def great_vect(lon1, lat1, lon2, lat2):
    """
    return the distance (km) between points following a great circle

    based on : https://gist.github.com/gabesmed/1826175

    >>> great_vect(0, 55, 8, 45.5)
    1199.3240879770135
    """

    earth_circumference = 6378137     # earth circumference in meters

    dlat = np.radians(lat2 - lat1)
    dlon = np.radians(lon2 - lon1)
    a = (np.sin(dlat / 2) * np.sin(dlat / 2) +
         np.cos(np.radians(lat1)) * np.cos(np.radians(lat2)) *
         np.sin(dlon / 2) * np.sin(dlon / 2))
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    distance = earth_circumference * c

    return distance/1000


def row_col_from_condensed_index(d, i):
    """return the row col index of the points used for the condensed matrix
    http://stackoverflow.com/questions/5323818/condensed-matrix-
    function-to-find-pairs/14839010#14839010
    """
    b = 1 - 2 * d
    x = np.floor((-b - np.sqrt(b**2 - 8*i))/2)
    y = i + x*(b + x + 2)/2 + 1
    if type(x) == np.ndarray:
        return x.astype(int), y.astype(int)
    else:
        return int(x), int(y)


def normal_point(pt1, pt2):
    """Return the vector normal to (pt1 - pt2), clockwise"""
    v = np.diff([pt1, pt2], axis=0)[0]
    normal = np.array([[0, -1], [1, 0]]).dot(v)
    return normal


def calculate_normal_vectors(points=None, filename=None, redo=False):
    """Calculate the normal vectors to a line

    Parameters
    ---------
    points: array of shape (N, 2)
        Each points of the line
    filename: string
        Filename where normal_vectors are saved;
        By default read from ../data/normal_vectors, save for the 300km line
    redo: bool
        If True redo the calculations of the normal_vectors
    """
    if filename is None:
        dirname = Path(__file__).realpath().dirname()
        filename = Path.joinpath(dirname, '..', 'data',
                                 'normal_vectors.npy')
    if redo:
        if points is None:
            points = startf_points_to_rotated_points()
        spoints = np.concatenate((points[1:, :], points[:1, :]))
        normal_vectors = np.array([normal_point(pt1, pt2)
                                   for pt1, pt2 in zip(points, spoints)])
        np.save(filename, normal_vectors)
    else:
        normal_vectors = np.load(filename)
    return normal_vectors


def composante_along_normal(vectors, wind):
    """Return the composant of wind on vectors"""
    nwind = np.linalg.norm(wind)
    nvectors = np.linalg.norm(vectors)
    if nwind == 0:
        return 0
    composante = nwind * (vectors.dot(wind) / (nwind * nvectors))
    if np.isnan(composante):
        return 0
    return composante


def max_heights_along_lines(lines=None, filename=None, redo=False):
    if filename is None:
        dirname = Path(__file__).realpath().dirname()
        filename = Path.joinpath(dirname, '..', 'data',
                                 'max_heights.npy')
    if redo:
        dirname = Path(__file__).realpath().dirname()
        cstfile = Path.joinpath(dirname, '..', 'data', 'LMCONSTANTS')
        distancefile = Path.joinpath(dirname, '..', 'data',
                                     'distance_grid_points_alps.cdf')
        try:
            hsurf, = read_var(cstfile, 'ZB')
        except KeyError:
            hsurf, = read_var(cstfile, 'HSURF')
        rlon, rlat = read_var(distancefile, ['lon', 'lat'])
        lo = np.array([rlat.min(), rlon.min()])
        hi = np.array([rlat.max(), rlon.max()])
        intfunc = Intergrid(hsurf, lo=lo, hi=hi, verbose=False)

        if lines is None:
            points = startf_points_to_rotated_points()
            normal_vectors = calculate_normal_vectors(points=points)
            lines = vectors_to_lines(normal_vectors, points)
            polygon = Polygon(points)
            lines = np.hstack([np.array(LineString(line).intersection(polygon))
                               for line in lines])

        def max_height_along(line, intfunc):
            ptx = np.linspace(*line[:, 0], num=1000)
            pty = np.linspace(*line[:, 1], num=1000)
            query_points = [[lat, lon] for lat, lon in zip(pty, ptx)]
            return max(intfunc.at(query_points))

        heights = [max_height_along(line, intfunc) for line in lines]
        np.save(filename, heights)
    else:
        heights = np.load(filename)

    return heights


def vectors_to_lines(vectors, points):
    """Transform ``vectors`` to : ``lines`` through ``points``"""
    lines = [[point.T - 100 * vector, point.T, point.T + 100 * vector]
             for point, vector in zip(points, vectors)]
    return np.array(lines)


def startf_points_to_rotated_points():
    slon, slat = return_startf()
    rot = ccrs.RotatedPole(pole_latitude=32.5, pole_longitude=-170)
    slons, slats, _ = rot.transform_points(ccrs.PlateCarree(), slon, slat).T
    return np.array(list(zip(slons, slats)))


def return_th(date):
    filename = '/net/thermo/atmosdyn/npiaget/projects/blocking.alps/' \
               'TH/{date:%Y}/{date:%m}/TH{date:%Y%m%d%H}.npy'
    return np.load(filename.format(date=date))

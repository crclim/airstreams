#!/bin/bash

env -i bash --norc -c "zsh -c '. /etc/profile.d/modules.sh;module load miniconda3;source activate dypy;export PATH=$PATH:/usr/bin
/:/bin/:/usr/local/bin;f2py3 -c -m hierarchical_clustering hierarchical_clustering.f90  --fcompiler=gfortran --f90flags='-fopenmp' -lgomp'"
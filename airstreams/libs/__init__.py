# f2py3 -c -m hierarchical_clustering hierarchical_clustering.f90  --fcompiler=gfortran --f90flags='-fopenmp' -lgomp
from .tools import *
try:
    import numba as nb
    from .numbatools import *
except ImportError:
    pass

from .hierarchical_clustering import mhc

hc = mhc.hc
dissmatrix = mhc.dissmatrix

__all__ = ['great_vect', 'hc', 'dissmatrix',
           'row_col_from_condensed_index']
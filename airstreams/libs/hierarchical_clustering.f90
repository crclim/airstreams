module mhc
  !$ use omp_lib
  implicit none

  public :: dissmatrix, hdissmatrix, hsdissmatrix, sdis, hc, hclass

contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C
!                                                            C
!  HIERARCHICAL CLUSTERING using (user-specified) criterion. C
!                                                            C
!  Parameters:                                               C
!                                                            C
!  DATA(N,M)         input data matrix,                      C
!  DISS(LEN)         dissimilarities in lower half diagonal  C
!                    storage; LEN = N.N-1/2,                 C
!  IOPT              clustering criterion to be used,        C
!  IA, IB, CRIT      history of agglomerations; dimensions   C
!                    N, first N-1 locations only used,       C
!  MEMBR, NN, DISNN  vectors of length N, used to store      C
!                    cluster cardinalities, current nearest  C
!                    neighbour, and the dissimilarity assoc. C
!                    with the latter.                        C
!  FLAG              boolean indicator of agglomerable obj./ C
!                    clusters.                               C
!                                                            C
!  F. Murtagh, ESA/ESO/STECF, Garching, February 1986.       C
!                                                            C
!------------------------------------------------------------C

      SUBROUTINE HC(N,IOPT,IA,IB,CRIT,MEMBR,NN,DISNN,FLAG,DISS)
        INTEGER, INTENT(IN) :: N
        INTEGER, INTENT(IN) :: IOPT
!f2py   INTENT(IN) :: DISS
        INTEGER, INTENT(OUT) :: IA(N), IB(N)
        REAL, INTENT(OUT) :: CRIT(N)
!f2py  INTENT(HIDE) :: MEMBR, DISNN, FLAG, NN
        LOGICAL FLAG(N)
        REAL DISS(N*(N-1)/2), DISNN(N), MEMBR(N), DMIN
        INTEGER J,K,JJ,IM,JM, NN(N), I, I2, IND, IND1, IND2, IND3, &
                J2, NCL
        REAL INF, X, XX
        DATA INF/1.E+20/
!
!  Initializations
!
  !$OMP PARALLEL DO
      DO I=1,N
         MEMBR(I)=1.
         FLAG(I)=.TRUE.
      ENDDO
  !$OMP END PARALLEL DO
      NCL=N
!
!  Carry out an agglomeration - first create list of NNs
!
      DO I=1,N-1
         DMIN=INF
         DO J=I+1,N
            IND=IOFFSET(N,I,J)
            IF (DISS(IND).GE.DMIN) GOTO 500
               DMIN=DISS(IND)
               JM=J
  500    CONTINUE
         ENDDO
         NN(I)=JM
         DISNN(I)=DMIN
      ENDDO
!
  400 CONTINUE
!     Next, determine least diss. using list of NNs
      DMIN=INF
      DO I=1,N-1
         IF (.NOT.FLAG(I)) GOTO 600
         IF (DISNN(I).GE.DMIN) GOTO 600
            DMIN=DISNN(I)
            IM=I
            JM=NN(I)
  600    CONTINUE
      ENDDO
      NCL=NCL-1
!
!  This allows an agglomeration to be carried out.
!
      I2=MIN0(IM,JM)
      J2=MAX0(IM,JM)
      IA(N-NCL)=I2
      IB(N-NCL)=J2
      CRIT(N-NCL)=DMIN
!
!  Update dissimilarities from new cluster.
!
      FLAG(J2)=.FALSE.
      DMIN=INF
      DO K=1,N
         IF (.NOT.FLAG(K)) GOTO 800
         IF (K.EQ.I2) GOTO 800
         X=MEMBR(I2)+MEMBR(J2)+MEMBR(K)
         IF (I2.LT.K) THEN
                           IND1=IOFFSET(N,I2,K)
                      ELSE
                           IND1=IOFFSET(N,K,I2)
         ENDIF
         IF (J2.LT.K) THEN
                           IND2=IOFFSET(N,J2,K)
                      ELSE
                           IND2=IOFFSET(N,K,J2)
         ENDIF
         IND3=IOFFSET(N,I2,J2)
         XX=DISS(IND3)
!
!  WARD'S MINIMUM VARIANCE METHOD - IOPT=1.
!
         IF (IOPT.EQ.1) THEN
            DISS(IND1)=(MEMBR(I2)+MEMBR(K))*DISS(IND1)+(MEMBR(J2)+MEMBR(K))*DISS(IND2)-MEMBR(K)*XX
            DISS(IND1)=DISS(IND1)/X
         ENDIF
!
!  SINGLE LINK METHOD - IOPT=2.
!
         IF (IOPT.EQ.2) THEN
            DISS(IND1)=MIN(DISS(IND1),DISS(IND2))
         ENDIF
!
!  COMPLETE LINK METHOD - IOPT=3.
!
         IF (IOPT.EQ.3) THEN
            DISS(IND1)=MAX(DISS(IND1),DISS(IND2))
         ENDIF
!
!  AVERAGE LINK (OR GROUP AVERAGE) METHOD - IOPT=4.
!
         IF (IOPT.EQ.4) THEN
            DISS(IND1)=(MEMBR(I2)*DISS(IND1)+MEMBR(J2)*DISS(IND2))/(MEMBR(I2)+MEMBR(J2))
         ENDIF
!
!  MCQUITTY'S METHOD - IOPT=5.
!
         IF (IOPT.EQ.5) THEN
            DISS(IND1)=0.5*DISS(IND1)+0.5*DISS(IND2)
         ENDIF
!
! MEDIAN (GOWER'S) METHOD - IOPT=6.
!
         IF (IOPT.EQ.6) THEN
            DISS(IND1)=0.5*DISS(IND1)+0.5*DISS(IND2)-0.25*XX
         ENDIF
!
!  CENTROID METHOD - IOPT=7.
!
         IF (IOPT.EQ.7) THEN
            DISS(IND1)=(MEMBR(I2)*DISS(IND1)+MEMBR(J2)*DISS(IND2)-MEMBR(I2)*MEMBR(J2)*XX &
            /(MEMBR(I2)+MEMBR(J2)))/(MEMBR(I2)+MEMBR(J2))
            ENDIF
!
         IF (I2.GT.K) GOTO 800
         IF (DISS(IND1).GE.DMIN) GOTO 800
            DMIN=DISS(IND1)
            JJ=K
  800    CONTINUE
      ENDDO
      MEMBR(I2)=MEMBR(I2)+MEMBR(J2)
      DISNN(I2)=DMIN
      NN(I2)=JJ
!
!  Update list of NNs insofar as this is required.
!
      DO I=1,N-1
         IF (.NOT.FLAG(I)) GOTO 900
         IF (NN(I).EQ.I2) GOTO 850
         IF (NN(I).EQ.J2) GOTO 850
         GOTO 900
  850    CONTINUE
!        (Redetermine NN of I:)
         DMIN=INF
         DO J=I+1,N
            IND=IOFFSET(N,I,J)
            IF (.NOT.FLAG(J)) GOTO 870
            IF (I.EQ.J) GOTO 870
            IF (DISS(IND).GE.DMIN) GOTO 870
               DMIN=DISS(IND)
               JJ=J
  870       CONTINUE
         ENDDO
         NN(I)=JJ
         DISNN(I)=DMIN
  900    CONTINUE
      ENDDO
!
!  Repeat previous steps until N-1 agglomerations carried out.
!
      IF (NCL.GT.1) GOTO 400
!
!
      RETURN
      END

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C
!                                                               C
!  Given a HIERARCHIC CLUSTERING, described as a sequence of    C
!  agglomerations, derive the assignments into clusters for the C
!  top LEV-1 levels of the hierarchy.                           C
!  Prepare also the required data for representing the          C
!  dendrogram of this top part of the hierarchy.                C
!                                                               C
!  Parameters:                                                  C
!                                                               C
!  IA, IB, CRIT: vectors of dimension N defining the agglomer-  C
!                 ations.                                       C
!  LEV:          number of clusters in largest partition.       C
!  HVALS:        veUtor of dim. LEV, used internally only.      C
!  ICLASS:       array of cluster assignments; dim. N by LEV.   C
!  IORDER, CRITVAL, HEIGHT: vectors describing the dendrogram,  C
!                all of dim. LEV.                               C
!                                                               C
!  F. Murtagh, ESA/ESO/STECF, Garching, February 1986.          C
!                                                               C
!---------------------------------------------------------------C
      SUBROUTINE HCLASS(N,IA,IB,CRIT,LEV,ICLASS,HVALS,IORDER,CRITVAL,HEIGHT)
     INTEGER, INTENT(IN) :: LEV, IA(N), IB(N)
     REAL, INTENT(IN) :: CRIT(N)
     INTEGER, INTENT(OUT) :: ICLASS(N,LEV)
!F2PY INTENT(HIDE) :: N
!F2PY INTENT(HIDE) :: HVALS, IORDER, CRITVAL, HEIGHT
      INTEGER HVALS(LEV),IORDER(LEV), HEIGHT(LEV), N, &
              I, J, ICL, K, ILEV, LEVEL, LOC, NCL
      REAL CRITVAL(LEV)
!
!  Pick out the clusters which the N objects belong to,
!  at levels N-2, N-3, ... N-LEV+1 of the hierarchy.
!  The clusters are identified by the lowest seq. no. of
!  their members.
!  There are 2, 3, ... LEV clusters, respectively, for the
!  above levels of the hierarchy.
!
      HVALS(1)=1
      HVALS(2)=IB(N-1)
      LOC=3
      DO 59 I=N-2,N-LEV,-1
         DO 52 J=1,LOC-1
            IF (IA(I).EQ.HVALS(J)) GOTO 54
  52     CONTINUE
         HVALS(LOC)=IA(I)
         LOC=LOC+1
  54     CONTINUE
         DO 56 J=1,LOC-1
            IF (IB(I).EQ.HVALS(J)) GOTO 58
  56     CONTINUE
         HVALS(LOC)=IB(I)
         LOC=LOC+1
  58     CONTINUE
  59  CONTINUE
!
      DO 400 LEVEL=N-LEV,N-2
         DO 200 I=1,N
            ICL=I
            DO 100 ILEV=1,LEVEL
  100       IF (IB(ILEV).EQ.ICL) ICL=IA(ILEV)
            NCL=N-LEVEL
            ICLASS(I,NCL-1)=ICL
  200    CONTINUE
  400  CONTINUE
!
      DO 120 I=1,N
      DO 120 J=1,LEV-1
      DO 110 K=2,LEV
      IF (ICLASS(I,J).NE.HVALS(K)) GOTO 110
         ICLASS(I,J)=K
         GOTO 120
  110 CONTINUE
  120 CONTINUE
!
!      WRITE (6,450)
!  450 FORMAT(4X,' SEQ NOS 2CL 3CL 4CL 5CL 6CL 7CL 8CL 9CL')
!      WRITE (6,470)
!  470 FORMAT(4X,' ------- --- --- --- --- --- --- --- --- ----')
!      DO 500 I=1,N
!      WRITE (6,600) I,(ICLASS(I,J),J=1,8)
!  600 FORMAT(I11,8I4)
  500 CONTINUE
!
!  Determine an ordering of the LEV clusters (at level LEV-1)
!  for later representation of the dendrogram.
!  These are stored in IORDER.
!  Determine the associated ordering of the criterion values
!  for the vertical lines in the dendrogram.
!  The ordinal values of these criterion values may be used in
!  preference, and these are stored in HEIGHT.
!  Finally, note that the LEV clusters are renamed so that they
!  have seq. nos. 1 to LEV.
!
      IORDER(1)=IA(N-1)
      IORDER(2)=IB(N-1)
      CRITVAL(1)=0.0
      CRITVAL(2)=CRIT(N-1)
      HEIGHT(1)=LEV
      HEIGHT(2)=LEV-1
      LOC=2
      DO 700 I=N-2,N-LEV+1,-1
         DO 650 J=1,LOC
            IF (IA(I).EQ.IORDER(J)) THEN
!              Shift rightwards and insert IB(I) beside IORDER(J):
               DO 630 K=LOC+1,J+1,-1
                  IORDER(K)=IORDER(K-1)
                  CRITVAL(K)=CRITVAL(K-1)
                  HEIGHT(K)=HEIGHT(K-1)
  630          CONTINUE
               IORDER(J+1)=IB(I)
                CRITVAL(J+1)=CRIT(I)
                HEIGHT(J+1)=I-(N-LEV)
               LOC=LOC+1
            ENDIF
  650   CONTINUE
  700 CONTINUE
      DO 705 I=1,LEV
         DO 703 J=1,LEV
            IF (HVALS(I).EQ.IORDER(J)) THEN
               IORDER(J)=I
               GOTO 705
            ENDIF
  703    CONTINUE
  705 CONTINUE
!
      RETURN
      END




      INTEGER FUNCTION IOFFSET(N,I,J)
!  Map row I and column J of upper half diagonal symmetric matrix
!  onto vector.
      INTEGER, INTENT(IN) :: N,I,J
      IOFFSET=J+(I-1)*N-(I*(I+1))/2
      RETURN
      END

!     ---------------------------------------------------------
!     Get spherical distance between lat/lon points
!     ---------------------------------------------------------

      real function sdis(xp,yp,xq,yq)

!     Calculates spherical distance (in km) between two points
!     given by their spherical coordinates (xp,yp) and (xq,yq),
!     respectively.

      real      re
      parameter (re=6378)
      real      xp,yp,xq,yq,arg
      real      pi180
      parameter (pi180 = 3.14159/180.)

      arg=sin(pi180*yp)*sin(pi180*yq)
      arg=arg+cos(pi180*yp)*cos(pi180*yq)*cos(pi180*(xp-xq))
      if (arg.lt.-1.) arg=-1.
      if (arg.gt.1.) arg=1.
      sdis=re*acos(arg)
      end

!     ---------------------------------------------------------
!     Create dissimilarity matrix
!     ---------------------------------------------------------

      subroutine dissmatrix(ntra, ntime, tra, d)
!f2py intent(in) tra
!f2py intent(hide), depend(tra) :: ntra=shape(tra, 0), ntime=shape(tra, 1)
!f2py intent(out), depend(ntra) :: d
      integer ntra, ntime, i, j, k, ind, nt
      real tra(ntra, ntime, 2), d(ntra*(ntra-1)/2), dist


      do i=1,ntra
         do j=i+1,ntra

            ind    = ioffset(ntra,i,j)
            d(ind) = 0.
          !$OMP PARALLEL DO
            do k=1,ntime
               dist=sdis(tra(i,k,1),tra(i,k,2),tra(j,k,1),tra(j,k,2))
               d(ind)=d(ind)+dist
            enddo
          !$OMP END PARALLEL DO

         enddo
      enddo
      end 
      
      
!     ---------------------------------------------------------
!     Create dissimilarity matrix with height
!     ---------------------------------------------------------

      subroutine hdissmatrix(ntra, ntime, tra, d)
!F2PY intent(in) tra
!F2PY intent(hide), depend(tra) :: ntra=shape(tra, 0), ntime=shape(tra, 1)
!F2PY intent(out), depend(ntra) :: d
      integer ntra, ntime, i, j, k, ind
      real tra(ntra, ntime, 3), d(ntra*(ntra-1)/2), dist, height
      print*, 'hdissmatrix'
      do i=1,ntra
         do j=i+1,ntra

            ind    = ioffset(ntra,i,j)
            d(ind) = 0.
            !$OMP PARALLEL DO
            do k=1,ntime
               dist=sdis(tra(i,k,1),tra(i,k,2),tra(j,k,1),tra(j,k,2))
               height = abs(tra(i, k, 3) - tra(j, k, 3))
               d(ind)=d(ind)+dist + height
            enddo
            !$OMP END PARALLEL DO
         enddo
      enddo
      end

!     ---------------------------------------------------------
!     Create dissimilarity matrix with height, short
!     ---------------------------------------------------------

      subroutine hsdissmatrix(ntra, ntime, tra, d)
!F2PY intent(in) tra
!F2PY intent(hide), depend(tra) :: ntra=shape(tra, 0), ntime=shape(tra, 1)
!F2PY intent(out), depend(ntra) :: d
      integer ntra, ntime, i, j, k, ind
      real tra(ntra, ntime, 3), d(ntra*(ntra-1)/2), dist, height
      print*, 'hdissmatrix'
      do i=1,ntra
         do j=i+1,ntra

            ind    = ioffset(ntra,i,j)
            d(ind) = 0.
            !$OMP PARALLEL DO
            do k=ntime/3,ntime-ntime/3
               dist=sdis(tra(i,k,1),tra(i,k,2),tra(j,k,1),tra(j,k,2))
               height = abs(tra(i, k, 3) - tra(j, k, 3))
               d(ind)=d(ind)+dist + height
            enddo
            !$OMP END PARALLEL DO

         enddo
      enddo
      end

end module mhc

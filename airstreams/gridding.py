from functools import partial
from multiprocessing.pool import Pool

import numpy as np
from dypy.netcdf import read_var
from dypy.tools.dyntools import run_cmd, DynToolsException
from path import Path


def gridding(trajs, outfile, var=None, workingdir='.', **kwargs):
    """Perform gridding of the trajs

    Parameters
    ----------
    trajs: Tra object
        trajectories with at least lat, lon and var (if given)
    outfile: string
        filename of the output file (add .npz if not given)
    var: string, default None
        name of the variable to use for the wheights
    debug: bool, default False
        If True standard output from gridding are not deleted
    diff: bool, default False
        If True, gridding is calulcated for the difference with start values;
        data = trajs[var] - trajs[var][:, 0:1]
    kwargs: keywords arguments
        to be passed to the gridding dyn_tools
    mdv: float
        Missing data value, default to -999.999
    """
    debug = kwargs.get('debug', False)
    diff = kwargs.pop('diff', False)
    mdv = kwargs.pop('mdv', -999.999)
    baseoutfile = Path.joinpath(workingdir, outfile.namebase)
    outfile_txt = Path(baseoutfile + '.txt')
    indices = np.where(~np.isnan(trajs['lon']))
    if var:
        if diff:
            data = trajs[var] - trajs[var][:, 0:1]
        else:
            data = trajs[var]
        vardata = data[indices].astype(trajs['lon'].dtype)
        array = np.c_[trajs['lon'][indices], trajs['lat'][indices], vardata]
    else:
        array = np.c_[trajs['lon'][indices], trajs['lat'][indices]]
    np.savetxt(outfile_txt, array, fmt='%1.3f', delimiter=' ')
    out = run_gridding(outfile_txt, outfile.namebase,
                       workingdir=workingdir, **kwargs)
    cstoutfile = Path(baseoutfile + '.cdf_cst')
    # change the cdf output to a ascii gz
    cdfoutfile = Path(baseoutfile + '.cdf')
    txtoutfile = Path(baseoutfile + '.npz')
    varnames = ['N', 'VAR', 'SD']
    try:
        vardata = read_var(cdfoutfile, varnames)
    except OSError:
        raise DynToolsException(out)
    vardata = [data.filled(fill_value=mdv) for data in vardata]
    vardict = dict(zip(varnames, vardata))
    coos = lons_lats_from_cst(cstoutfile)
    vardict.update(zip(['lons', 'lats'], coos))
    np.savez_compressed(txtoutfile, **vardict)
    if not debug:
        # remove input file of the gridding tool
        outfile_txt.remove()
        cdfoutfile.remove()
        # remove the constant file unneeded for space
        cstoutfile.remove()
    return out


def lons_lats_from_cst(filename):
    varnames = ['lonmin', 'lonmax', 'latmin', 'latmax',
                'dellon', 'dellat']
    lonmin, lonmax, latmin, latmax, dlon, dlat = read_var(filename, varnames)
    lon = np.arange(lonmin, lonmax + dlon, dlon)
    lat = np.arange(latmin, latmax + dlat, dlat)
    return np.meshgrid(lon, lat)


def run_gridding(filename, outfile, r=500, grid=(-180, 180, -90, 90),
                 dx=1, dy=1, options='', workingdir='.', **kwargs):
    """Wrapper to run the gridding tools from Bojan"""
    gridding_cmd = 'gridding {options} {inputf} {outf} {r} {grids} ' \
                   '{dy} {dx}'
    grids = " ".join([str(pt) for pt in grid])
    gridding_cmd = gridding_cmd.format(inputf=filename, outf=outfile, r=r,
                                       grids=grids, dx=dx, dy=dy,
                                       options=options)
    debug = kwargs.pop('debug', False)
    if debug:
        print('\n\nrun_gridding debug:\n')
        print(workingdir)
        print(gridding_cmd)
        print('--------------------\n')
    out = run_cmd(gridding_cmd, workingdir=workingdir, **kwargs)
    if ('Syntax' in out) or ('Gridding error' in out):
        raise DynToolsException(gridding_cmd + '\n' + out)
    return out


def read_gridding_file(filename, var='N', nanvalues=-999.999):
    """ return the results from gridding

    Parameters
    ----------
    filename: string,
        Path to the result
    var: string,
        can be one of 'N', 'VAR', 'SD'

    Returns
    -------
    array
    """
    read_single = partial(read_single_gridding, var=var, nanvalues=nanvalues)

    if type(filename) is not list:
        results = read_single(filename)
    else:
        with Pool(processes=10) as pool:
            results = pool.map(read_single, filename)
        results = np.stack(results)
    return results


def read_single_gridding(fname, var, nanvalues=None):
    with np.load(fname) as data:
        vardata = data[var]
    if nanvalues is not None:
        vardata[np.where(vardata == nanvalues)] = np.nan
    return vardata


def return_grid_coords(filename):
    """return lons, lats from gridding result file"""
    lons = read_single_gridding(filename, 'lons')
    lats = read_single_gridding(filename, 'lats')
    return lons, lats

# coding: utf-8
"""airstreams.py

Description:
    Definition of the Flow class and of functions to determine them from
    the trajectories
"""

import netCDF4
import numpy as np
from dypy.constants import g
from dypy.intergrid import Intergrid
from dypy.lagranto import LagrantoRun
from dypy.lagranto import Tra
from dypy.netcdf import read_var, read_gattributes
from dypy.small_tools import rotate_points
from dypy.tools.dyntools import DynToolsException
from path import Path
from tempfile import mkdtemp

from airstreams.libs.tools import composante_along_normal, \
    calculate_normal_vectors, max_heights_along_lines, return_th
from .clustering import HierarchicalClustering
from .gridding import gridding
from .libs import great_vect, row_col_from_condensed_index, dissmatrix


def return_spatial_extent(trajs):
    return great_vect(trajs[:-1, 0]['lon'], trajs[:-1, 0]['lat'],
                      trajs[1:, 0]['lon'], trajs[1:, 0]['lat']).sum()


class Flow(Tra):
    """ Class that defined an Flow.

    Descriptions:
        A Flow is an coherent ensemble of trajectories separated by less than
        a <neighbour> distance (default: 200km, see return_connected_trajs).
        The trajectories have to approach the alps by less than a <distance>
        (default: 250km, see return_close_enough_trajs).

    Args:
        Flow.clusters:  Variable with an unique value for each trajectory
                        defining its cluster.
        Flow.numbers:   Give the number of the flow and the amount of other
                        flows with the same initial date.
    """

    extent = 400

    _characteristics = ('spatial_extent', 'mean_lon', 'mean_lat',
                        'mean_wind_speed', 'mean_wind_direction',
                        'mean_wind_normal', 'mean_froude_number',
                        'mean_angle_normal',
                        'inner_alpine_percentile', 'max_topography',
                        'mean_final_spread', 'max_amplification',
                        'max_dz', 'min_dqv', 'sum_tot_prec',
                        'min_dth', 'min_dthe', 'max_dth', 'max_dthe',
                        'min_wind_speed', 'max_wind_speed', 'max_w',
                        'start_positions')

    def __init__(self, *args, array=None, **kwargs):
        super().__init__(*args, **kwargs)
        for attr in self._characteristics:
            self.__dict__[attr] = None
        self.numbers = '1/1'
        self.clusters = None
        self.Z = None
        self.normal_wind = None
        self.th = None
        if array is not None:
            self.set_array(array)

    @property
    def check_spatial_extent(self):
        return check_spatial_extent(self, self.extent)

    @property
    def characteristics(self):
        return dict((attr, self.__dict__[attr])
                    for attr in self._characteristics)

    def _mean_froude_number(self):
        """Return the mean froude number

        Fr = (N*H) / W, with
            W normal wind (never negative)
            N brunt vaïsaïla frequency
            H the maximum height along the normal
        """
        numbers = self['number'][:, 0].astype(int)
        if self.normal_wind is None:
            self._mean_wind_normal()
        wind = np.array(self.normal_wind)
        wind[wind < 0] = np.nan
        h = max_heights_along_lines()[numbers]
        if self.th is None:
            try:
                self.th = return_th(self.startdate)
            except FileNotFoundError:
                return None
        ths = self.th[:,  numbers]
        n = np.sqrt((g / ths[1, :]) * np.diff(ths[[0, -1], :], axis=0) / 1000)
        n = n.squeeze()
        with np.errstate(divide='ignore', invalid='ignore'):
            froude = (n * h) / wind
        froude[np.isinf(froude)] = np.nan
        return froude, np.nanmean(froude)

    def _mean_wind_normal(self):
        """Return mean wind speed normal to the 300km line towards the Alps"""
        if self.normal_wind is None:
            numbers = self['number'][:, 0].astype(int)
            normal_vectors = calculate_normal_vectors()
            wind = np.stack((self['U'][:, 0], self['V'][:, 0]), axis=1)
            self.normal_wind = [composante_along_normal(n, w)
                                for n, w in zip(normal_vectors[numbers], wind)]
        return np.mean(self.normal_wind)

    def _mean_angle_normal(self):
        """ Return the wind angle to the normal

        The angle is positif to the left of the normal
        """
        wind = np.stack((self['U'][:, 0], self['V'][:, 0]), axis=1)
        norm = np.linalg.norm(wind, axis=1)
        adjac = self.normal_wind
        with np.errstate(divide='ignore', invalid='ignore'):
            opp = norm * np.sin(np.arccos(adjac / norm))
        numbers = self['number'][:, 0].astype(int)
        normals = calculate_normal_vectors()[numbers]
        sign = np.sign(
            normals[:, 0] * wind[:, 1] - normals[:, 1] * wind[:, 0])
        angles = sign * 180 * np.arctan2(adjac, opp) / np.pi
        return np.nanmean(angles)

    def _spatial_extent(self):
        spatial_extent = return_spatial_extent(self)
        return spatial_extent

    def _mean_lon(self):
        mean_lon = self['lon'][:, 0].mean()
        return mean_lon

    def _mean_lat(self):
        mean_lat = self['lat'][:, 0].mean()
        return mean_lat

    def _mean_wind_speed(self):
        u = np.nanmean(self['U'][:, 0])
        v = np.nanmean(self['V'][:, 0])
        return np.sqrt(u**2 + v**2)

    def _mean_wind_direction(self):
        def wind_angle(wind):
            origin = np.array([0, 0]) - wind
            angle = np.arctan2(*origin[::-1])
            return np.rad2deg((np.pi / 2 - angle) % (2 * np.pi))
        u = np.nanmean(self['U'][:, 0])
        v = np.nanmean(self['V'][:, 0])
        return wind_angle(np.array([u, v]))

    def set_numbers(self, number, total):
        self.numbers = '{}/{}'.format(number, total)

    def _inner_alpine_percentile(self):
        """Return info for individual trajs and for the flow

        Return the percentage of time step over the inner alps for each trajs
        and the number of trajs crossing the Alps for the flow.
        """
        indices = np.where(self['dist'] < 0)

        def get_traj_perc(i):
            number = indices[1][np.where(indices[0] == i)].size
            if number > 0:
                return number / self.ntime
            else:
                return 0

        perc = [get_traj_perc(i) for i in range(self.ntra)]
        return perc,  np.nonzero(perc)[0].size / len(perc)

    def _max_topography(self):
        try:
            max_topography = self['ZB'].max(axis=1)
        except ValueError:
            max_topography = self['HSURF'].max(axis=1)
        return max_topography, np.median(max_topography)

    def _mean_final_spread(self):
        spoints = self[:, -1][['lon', 'lat']].view(self.dtype[1])
        spoints = spoints.reshape((self.ntra, 1, -1))
        distances = dissmatrix(spoints)
        mean_final_spread = np.median(distances)
        return mean_final_spread

    def _max_amplification(self, mindistance=50):
        """return the maximum amplification

        amplification is the dist/dist0, where dist0 is the horizontal distance
        between two trajectories at their starting point, need to be lower than
        <mindistance>, and dist is the maximal distance between the trajectories
        """
        spoints = self[:, 0][['lon', 'lat']].view(self.dtype[1])
        spoints = spoints.reshape((self.ntra, 1, -1))
        distances = dissmatrix(spoints)
        index = np.where(distances < mindistance)[0]
        trajs_index = row_col_from_condensed_index(spoints.shape[0], index)
        allpoints = self[['lon', 'lat']][np.vstack(trajs_index).T, :]
        maxdistances = great_vect(allpoints[:, 0]['lon'],
                                  allpoints[:, 0]['lat'],
                                  allpoints[:, 1]['lon'],
                                  allpoints[:, 1]['lat']).max(axis=1)
        return (maxdistances / distances[index]).max()

    def _max_dz(self):
        max_dz = np.diff(self['z']).max(axis=1)
        return max_dz, np.median(max_dz)

    def _min_dqv(self):
        min_dqv = np.diff(self['QV']).min(axis=1)
        return min_dqv, np.median(min_dqv)

    def _sum_tot_prec(self):
        """return sum of tot_prec in mm in 24 hours"""
        precip = self['TOT_PREC'].sum(axis=1) / 6  # from 10min to hours
        return precip, np.median(precip)

    def _min_dth(self):
        return np.median((self['TH'] - self['TH'][:, 0:1]).min(axis=1))

    def _min_dthe(self):
        return np.median((self['THE'] - self['THE'][:, 0:1]).min(axis=1))

    def _max_dth(self):
        return np.median((self['TH'] - self['TH'][:, 0:1]).max(axis=1))

    def _max_dthe(self):
        return np.median((self['THE'] - self['THE'][:, 0:1]).max(axis=1))

    def _min_wind_speed(self):
        min_wind = np.sqrt(self['U'] ** 2 + self['V'] ** 2).min(axis=1)
        return min_wind, np.median(min_wind)

    def _max_wind_speed(self):
        max_wind = np.sqrt(self['U'] ** 2 + self['V'] ** 2).max(axis=1)
        return max_wind, np.median(max_wind)

    def _max_w(self):
        max_w = self['W'].max(axis=1)
        return max_w, np.median(max_w)

    def _start_positions(self):
        numbers = self['number'][:, 0]
        start_positions = '{}-{}'.format(int(numbers.min()),
                                         int(numbers.max()))
        return start_positions

    def characterize(self):
        for attr in self._characteristics:
            characteristic = eval('self._{}()'.format(attr))
            if type(characteristic) is tuple:
                self[attr] = np.repeat(np.array([characteristic[0]]).T,
                                       self.ntime, axis=1)
                characteristic = characteristic[1]
            self.__dict__[attr] = characteristic

    # TODO ASHT
    # TODO characterize the meteo in the inner alpine region
    #       using sub regions?

    def write_netcdf(self, filename, **kwargs):
        exclude = kwargs.pop('exclude', [])
        exclude.append('Z')
        super().write_netcdf(filename, exclude=exclude, **kwargs)
        with netCDF4.Dataset(filename, 'a') as ncfile:
            if self.Z is not None:
                ncfile.createDimension('nlink', size=4)
                ncfile.createDimension('nZ', size=self.ntra-1)
                var = ncfile.createVariable('Z', self.Z.dtype, ('nZ', 'nlink'))
                var[:] = self.Z
            ncfile.setncatts({'numbers': self.numbers})
            ncfile.sync()

    def load_netcdf(self, filename, **kwarg):
        exclude = kwarg.pop('exclude', [])
        exclude.append('Z')
        super().load_netcdf(filename, exclude=exclude, **kwarg)
        self.set_numbers(*read_gattributes(filename)['numbers'].split('/'))
        try:
            with netCDF4.Dataset(filename, 'r') as ncfile:
                self.Z = ncfile.variables['Z'][:]
            labels = self['cluster'][:, 0]
            clusters = np.unique(labels)
            self.clusters = []
            for cl in clusters:
                index = np.where(labels == cl)
                airstream = AirStream(self._array[np.unique(index[0]), :], cl)
                self.clusters.append(airstream)
        except KeyError:
            pass

    def clustering(self, ncluster=3, linkage='ward', method='standard'):
        """Clustering of the flow

        Define:
            clusters: A list of AirStream

        based on the distance between the trajectories
        """
        hc = HierarchicalClustering(ncluster=ncluster, method=method,
                                    linkage=linkage)
        hc.fit(self)
        self.Z = hc.Z
        self['cluster'] = np.zeros(self.shape)*np.nan
        self.clusters = []
        for cl in np.arange(1, ncluster+1):
            index = np.where(hc.labels_ == cl)
            airstream = AirStream(self._array[np.unique(index[0]), :], cl)
            self.clusters.append(airstream)
            self['cluster'][index] = cl

    def mean(self):
        ntrajs = Tra()
        ntrajs.set_array(np.zeros((1, self.shape[1]), dtype=self.dtype))
        ntrajs['time'] = self['time'][0, :]
        for var in self.variables:
            if var == 'time':
                continue
            ntrajs[var] = np.median(self[var], axis=0)
        return ntrajs

    def gridding(self, filename, var=None, **kwargs):
        """Grid the Flow on the COSMO grid

        Parameters
        ----------
        filename: string,
            Filename, without extension where gridding is saved as netcdf
        var: string, default None
            Variable name to use for the weights
        kwargs: dict
            arguments pass to the gridding tool (dyn_tools)

        """
        out = gridding(self, filename, var=var, **kwargs)
        return out


class AirStream(Flow):

    def __init__(self, trajs, label):
        super().__init__()
        self.set_array(trajs)
        self.label = label


def return_close_enough_trajs(trajs, distance=250):
    """return trajectories less than <distance> away from the Alps"""
    selected_trajs = Tra()
    index = np.where(trajs['dist'].min(axis=1) < distance)
    selected_trajs.set_array(trajs[index[0], :])
    return selected_trajs


def return_connected_trajs(trajs, neighbour=200):
    """Return a list of  trajectories which are neighbours at the starting time.

    Description:
        Two trajectories are neighbours if their distance
        at time 0 is lower than <neighbour>.
    """
    trajs1 = trajs[:, 0]
    trajs2 = trajs[list(range(1, trajs.ntra)) + [0], 0]
    distances = great_vect(trajs1['lon'], trajs1['lat'],
                           trajs2['lon'], trajs2['lat'])
    start_indices = np.where((distances > neighbour))[0]
    indices = np.split(np.arange(trajs.ntra), start_indices + 1)
    if distances[-1] < neighbour:
        indices[0] = np.r_[indices[0], indices.pop()]
    return [trajs[ind, :] for ind in indices if ind.size > 0]


def check_spatial_extent(trajs, extent=400):
    """Check if the spatial extent is big enough the start position

    Parameters
    ----------
    trajs: Tra,
        Ensemble of trajectories, flow candidate
    extent: float
        Minimal spatial extent between the first and last flow's trajectories

    Returns
    -------
    boolean
    """
    trajs_extent = return_spatial_extent(trajs)
    if trajs_extent < extent:
        return False
    else:
        return True


def return_flows(trajs, extent=400, distance=250, neighbour=200):
    """Return flows from trajectories.

    Check if the conditions for flows are met by the trajs
    and return a list of flows is they are met.
    Raise a DynToolsException if no flow is found

    Parameters
    ----------
    trajs: Tra,
        A set of trajectories
    extent: float
        The minimal spatial extent of the starting position of a flow
    distance: float,
        The minimal distance from the Alps that a flow as to reach at least once
    neighbour: float,
        The maximal distance bewteen two neighbouring trajectories in a flow

    Returns
    -------
    list:
        List of flow feature
        A DynToolsException is raised when no flow can be found
    """
    if 'dist' not in trajs.variables:
        trajs = set_dist_from_alps(trajs)

    if 'number' not in trajs.variables:
        trajs['number'] = np.arange(trajs.ntra, dtype=np.float64).reshape(
            (trajs.ntra, 1)).repeat(trajs.ntime, axis=1)

    try:
        selected_trajs = return_close_enough_trajs(trajs, distance=distance)
    except ValueError as err:
        err.args += (trajs.startdate, )
        raise

    if not selected_trajs.size:
        msg = 'Not close enough: {:%Y%m%d%H}'.format(trajs.startdate)
        raise DynToolsException(msg)

    connected_trajs = return_connected_trajs(selected_trajs,
                                             neighbour=neighbour)
    if not connected_trajs:
        msg = 'No connected trajs: {:%Y%m%d%H}'.format(trajs.startdate)
        raise DynToolsException(msg)

    flows = [Flow(array=trajs) for trajs in connected_trajs
             if check_spatial_extent(trajs, extent=extent)]

    if not flows:
        msg = 'Spatial extents < {}: {:%Y%m%d%H}'.format(extent,
                                                        trajs.startdate)
        raise DynToolsException(msg)
    return flows


def set_dist_from_alps(trajs, distancefile=None, pollon=-170, pollat=32.5):
    if distancefile is None:
        dirname = Path(__file__).realpath().dirname()
        distancefile = Path.joinpath(dirname, 'data',
                                     'distance_grid_points_alps.cdf')
        distancefile = distancefile.abspath()
    rlon, rlat, dist = read_var(distancefile, ['lon', 'lat', 'DISTANCE'],
                                aggdim='lon')
    trlon, trlat = rotate_points(pollon, pollat, trajs['lon'],
                                 trajs['lat'])
    rtrajs = Tra()
    rtrajs.set_array(np.zeros(trajs.shape, dtype=[('rlon', trajs.dtype[1]),
                                                  ('rlat', trajs.dtype[2])]))
    rtrajs['rlat'] = trlat.T
    rtrajs['rlon'] = trlon.T
    hi = np.array([rlat.max(), rlon.max()])
    lo = np.array([rlat.min(), rlon.min()])
    interfunc = Intergrid(dist, lo=lo, hi=hi, verbose=False)
    query_points = rtrajs[['rlat', 'rlon']].view(trajs.dtype[1]).reshape(-1, 2)
    query_points = query_points.reshape(-1, 2)
    distances = interfunc.at(query_points)
    trajs['dist'] = distances.reshape(trajs.shape)
    if 'dist' not in trajs.variables:
        raise DynToolsException(str(distancefile))
    return trajs


class RunFlows(LagrantoRun):
    """Calculate flows

    """

    def __init__(self, *args, distance=250, neighbour=200, extent=400,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self.neighbour = neighbour
        self.distance = distance
        self.extent = extent

    def single_run(self, sd, ed, debug=False, caltra_kw=None, flow_kw=None,
                   **kwargs):
        if caltra_kw is None:
            caltra_kw = dict()
        if flow_kw is None:
            flow_kw = dict()

        workingdir = self.workingdir
        no_tmp_dir = caltra_kw.get('no_tmp_dir', False)
        if no_tmp_dir:
            tmpdir = workingdir
        else:
            tmpdir = Path(mkdtemp())
            self.workingdir = tmpdir
            nkwargs = {'version': self.version,
                       'outputdir': self.outputdir,
                       'sdate': self.sdate}
            nkwargs.update(kwargs)
            self.link_files(workingdir, tmpdir, **nkwargs)

        caltrakw = {'withtrace': True}
        caltrakw.update(caltra_kw)

        try:
            out = self.caltra(sd, ed, **caltrakw)
        except DynToolsException as err:
            err.args += (tmpdir,)
            if debug:
                raise
            else:
                return err
        try:
            out += self.is_flow(sd, **flow_kw)
        except DynToolsException as err:
            err.args += (tmpdir,)
            if debug:
                raise
            else:
                return err

        self.workingdir = workingdir
        if not no_tmp_dir:
            tmpdir.rmtree_p()
        return out

    def is_flow(self, date, unit='seconds'):
        """
        Parameters
        ----------
        date: datetime object,
            Date for which to test if the trajectories form a flow
        unit: string, optional.
            Time unit of the lsl file§
        """

        filename = Path.joinpath(self.outputdir, self.lslname.format(date))
        trajs = Tra()
        try:
            trajs.load_netcdf(filename, unit=unit)
        except (IndexError, FileNotFoundError, RuntimeError) as err:
            raise DynToolsException('{}:\t{}:{}'.format(filename,
                                                        type(err), err))

        try:
            flows = return_flows(trajs, extent=self.extent,
                                 distance=self.distance,
                                 neighbour=self.neighbour)
        except DynToolsException as err:
            return '{} has no flow:\t {}'.format(filename, err.args[0])
        except Exception as err:
            return '{}: error {}'.format(filename, err)

        outflow = Path.splitext(filename)[0]
        nbre_flow = len(flows)
        for i, flow in enumerate(flows):
            flow.set_numbers(i + 1, nbre_flow)
            flow.write_netcdf(outflow + '_{:03d}.nc'.format(i + 1),
                              unit='seconds')

        return '{} has {} flows'.format(filename, nbre_flow)

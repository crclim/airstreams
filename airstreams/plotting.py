# coding: utf-8
"""plotting function for Flow

"""
import matplotlib as mpl

import matplotlib.pyplot as plt
import numpy as np
import os
from dypy.netcdf import read_var, read_gattributes
from dypy.small_tools import rotate_points
from dypy.plotting import plot_trajs
from matplotlib.colors import ListedColormap
from path import Path
import cartopy.crs as ccrs

datapath = Path.dirname(Path(__file__)).joinpath('data/')
lmconstant = datapath / 'LMCONSTANTS'

try:
    mpl_rc_fname = os.environ['PAPER']
    param = mpl.rc_params_from_file(mpl_rc_fname, use_default_template=False)
    param.pop('backend')
    mpl.rcParams.update(param)
except KeyError:
    pass


def return_height(filename=lmconstant):
    """ Return the heights of the COSMO model

        Default for COSMO 7

        Return:
            lon, lat, z
    """
    gattributes = read_gattributes(filename)
    pollon = gattributes['pollon']
    pollat = gattributes['pollat']
    rlon, rlat, z = read_var(filename, ['lon', 'lat', 'ZB'])
    rlons, rlats = np.meshgrid(rlon, rlat)
    lon, lat = rotate_points(pollon, pollat, rlons, rlats, direction='r2n')
    return lon.T, lat.T, z


def plot_airstreams(ax, m, flow,
                    colors=('#e41a1c', '#377eb8', '#4daf4a', '#984ea3'),
                    z=None, z_interval=np.arange(500, 3500, 1000),
                    lon=None, lat=None, startf=False):
    """Plot the airstreams of a Flow"""
    m.ax = ax
    m.drawmap(coastargs={'linewidth': 0.6})
    ncluster = int(flow['cluster'].max())
    lc = m.plot_traj(flow, 'cluster', cmap=ListedColormap(colors[:ncluster]),
                     levels=np.arange(1, ncluster+2), lw=2, zorder=10)
    cb = plt.colorbar(lc, shrink=0.5, pad=0.01)
    cb.set_ticks(np.arange(1, ncluster + 1) + 0.5)
    cb.set_ticklabels(range(1, ncluster+1))
    cb.ax.tick_params(size=0)
    cb.set_label('Classes')
    if z is not None:
        x, y = m(lon, lat)
        m.contour(x, y, z, levels=z_interval,
                  colors=('silver', 'grey', 'k'),
                  linewidths=1.2)
    if startf:
        startf_file = datapath / 'startf.txt'
        slon, slat = np.genfromtxt(startf_file).T
        m.plot(slon, slat, '.', color='orange', latlon=True)

        
def return_startf():
    startf_file = datapath / 'startf.txt'
    slon, slat = np.genfromtxt(startf_file).T
    return slon, slat


def plot_trajectories(ax, flow, z=None,
                      z_interval=np.arange(500, 3500, 1000), lon=None,
                      lat=None, startf=False):
    """Plot the airstreams of a Flow
    ax need to be a GeoAx from cartopy
    """
    lc = plot_trajs(ax, flow, 'z', cmap='Spectral',
                      levels=np.arange(200, 3300, 200), lw=2, zorder=10)
    cb = plt.colorbar(lc, shrink=0.5, pad=0.01)
    cb.set_label('z')
    if z is not None:
        ax.contour(lon, lat, z, levels=z_interval,
                   colors=('silver', 'grey', 'k'),
                   linewidths=1.2, transform=ccrs.PlateCarree())
    if startf:
        slon, slat = return_startf()
        ax.plot(slon, slat, '.', color='orange', transform=ccrs.PlateCarree())
